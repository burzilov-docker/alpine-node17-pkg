FROM node:17-alpine

RUN npm install -g pkg pkg-fetch

USER node
WORKDIR /home/node
RUN pkg-fetch
